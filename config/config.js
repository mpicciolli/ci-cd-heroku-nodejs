var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'ci-cd-heroku-nodejs'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/ci-cd-heroku-nodejs-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'ci-cd-heroku-nodejs'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/ci-cd-heroku-nodejs-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'ci-cd-heroku-nodejs'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/ci-cd-heroku-nodejs-production'
  }
};

module.exports = config[env];
